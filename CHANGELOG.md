## v2.0.1

1. Verified on DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)
2. Xutils fixes cache read/write errors.

## v2.0.0

1. Compatible with DevEco Studio: 4.1 Canary(4.1.3.317), OpenHarmony SDK: API11(4.1.0.36)
2. Adaptation to new grammar

## v1.0.1

1. Compatible with DevEco Studio 3.1 Beta1(3.1.0.200).
2. Compatible with OpenHarmony SDK API 9(3.2.10.6).
3. Replace Fileio with fs for reading and writing files
4. Fix file download crash issue

## v1.0.0

-Implemented functions
1. Data request
2. File Download
3. File upload
4. Create a database
5. Create a table
6. Insert data
7. Query data
8. Update data
9. Delete data
10. Delete Table
11. Delete database
12. Load image
13. Retrieve images from cache
14. Clear local files
15. Network reconnection, request interceptor, Gzip compression, HTTPS processing

- Unrealized functionality

1. SSL secure connection and useragent configuration cannot be implemented. Currently, ETS's request interface supports options, but there are currently no key, certificate, ca, rejectUnauthorized and other attribute parameters in HttpRequestOptions