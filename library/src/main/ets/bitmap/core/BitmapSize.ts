/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class BitmapSize {
    public static ZERO: BitmapSize = new BitmapSize(0, 0);
    private width: number ;
    private height: number ;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }
    /**
     * Scales down dimensions in <b>sampleSize</b> times. Returns new object.
     */
    scaleDown(sampleSize: number): BitmapSize {
        return new BitmapSize(this.width / sampleSize, this.height / sampleSize);
    }

    /**
     * Scales dimensions according to incoming scale. Returns new object.
     */
    scale(scale: number): BitmapSize {
        return new BitmapSize((this.width * scale), (this.height * scale));
    }

    getWidth(): number {
        return this.width;
    }

    getHeight(): number {
        return this.height;
    }

    setWidth(value: number) {
        this.width = value
    }

    setHeight(value: number) {
        this.height = value;
    }

    toString(): String {
        return "_" + this.width + "_" + this.height;
    }
}
