/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { BitmapSize } from '../bitmap/core/BitmapSize'
import { BitmapFactory } from './factory/BitmapFactory'
import { Priority } from '../task/Priority'

export class BitmapDisplayConfig {
    private bitmapMaxSize: BitmapSize;
    private loadingDrawable: object;
    private loadFailedDrawable: PixelMap;
    private autoRotation = false;
    private showOriginal = false;
    private bitmapFactory: BitmapFactory;
    private priority: Priority;
    private mConfig: BitmapDisplayConfig;
    private mLoadingPixMap: PixelMap;
    private mLoadingResource: Resource;
    private mFailedResource: Resource;
    private mFailedPixelMap: PixelMap;

    public getBitmapMaxSize(): BitmapSize {
        return this.bitmapMaxSize == null ? BitmapSize.ZERO : this.bitmapMaxSize;
    }

    public setBitmapMaxSize(bitmapMaxSize: BitmapSize): void {
        this.bitmapMaxSize = bitmapMaxSize;
    }

    public getLoadingPixMapisNull(): boolean {
        let value = false;
        if (this.mLoadingPixMap == null || this.mLoadingPixMap == undefined) {
            value = true;
        } else {
            value = false;
        }
        return value;
    }

    public setLoadingPixMap(pix: PixelMap): void {
        this.mLoadingPixMap = pix;
    }

    public setLoadingDrawable(resource: Resource): void {
        this.mLoadingResource = resource;
    }

    public getLoadingPixMap(): PixelMap {
        return this.mLoadingPixMap;
    }

    public getLoadingDrawable(): Resource {
        return this.mLoadingResource;
    }

    public isAutoRotation(): boolean {
        return this.autoRotation;
    }

    public setAutoRotation(autoRotation: boolean): void {
        this.autoRotation = autoRotation;
    }

    public isShowOriginal(): boolean {
        return this.showOriginal;
    }

    public setShowOriginal(showOriginal: boolean): void {
        this.showOriginal = showOriginal;
    }

    public getBitmapFactory(): BitmapFactory {
        return this.bitmapFactory;
    }

    public setBitmapFactory(bitmapFactory: BitmapFactory): void {
        this.bitmapFactory = bitmapFactory;
    }

    public getPriority(): Priority {
        return this.priority;
    }

    public setPriority(priority: Priority): void {
        this.priority = priority;
    }

    public setLoadFailedPixelMap(pixelMap: PixelMap) {
        this.mFailedPixelMap = pixelMap;
    }

    public getLoadFailedPixelMap(): PixelMap {
        return this.mFailedPixelMap;
    }

    public getLoadFailedDrawable(): Resource {
        return this.mFailedResource;
    }

    public setLoadFailedDrawable(res: Resource): Resource {
        this.mFailedResource = res;
        return this.mFailedResource
    }

    public getLoadFailedPixMapisNull(): boolean {
        let value = false;
        if (this.mFailedPixelMap == null || this.mFailedPixelMap == undefined) {
            value = true;
        } else {
            value = false;
        }
        return value;
    }

    public cloneNew(): BitmapDisplayConfig {
        this.mConfig = new BitmapDisplayConfig();
        this.mConfig.bitmapMaxSize = this.bitmapMaxSize;
        this.mConfig.loadingDrawable = this.loadingDrawable;
        this.mConfig.loadFailedDrawable = this.loadFailedDrawable;
        this.mConfig.autoRotation = this.autoRotation;
        this.mConfig.showOriginal = this.showOriginal;
        this.mConfig.bitmapFactory = this.bitmapFactory;
        this.mConfig.priority = this.priority;
        this.mConfig.mLoadingPixMap = this.mLoadingPixMap
        this.mConfig.mLoadingResource = this.mLoadingResource
        this.mConfig.mFailedResource = this.mFailedResource
        this.mConfig.mFailedPixelMap = this.mFailedPixelMap
        return this.mConfig;
    }
}
