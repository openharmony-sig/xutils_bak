/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


export abstract class QueryCallBack {
    public abstract onSuccessCreateDb();

    public abstract onSuccessCreateTable();

    public abstract onSuccessInsert(ret: ESObject);

    public abstract onErrorInsert(err: ESObject);

    public abstract onSuccessQuery(resultSet: ESObject);

    public abstract onFailQuery(msg: string);

    public abstract onSuccessQueryFirst(map: Map<string, ESObject>);

    public abstract onSuccessUpdate(ret: ESObject);

    public abstract onErrorUpdate(err: ESObject);

    public abstract onSuccessDelete(rows: ESObject);

    public abstract onErrorDelete(err: ESObject);

    public abstract onSuccessDropTable();

    public abstract onSuccessDeleteDb();
}
