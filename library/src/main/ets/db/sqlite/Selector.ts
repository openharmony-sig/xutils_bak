/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import dataRdb from '@ohos.data.relationalStore'

export class Selector {
    public predicates
    private tableName: string;
    private queryColumns: Array<string>

    private constructor(tableName: string, columns: Array<string>) {
        this.tableName = tableName;
        this.queryColumns = columns;
        this.predicates = new dataRdb.RdbPredicates(this.tableName)
    }

    public static from(tableName: string, columns: Array<string>): Selector {
        return new Selector(tableName, columns);
    }

    public where(columnName: string, op: string, value: number | string | boolean): Selector {
        switch (op) {
            case "equalTo":
                this.predicates.equalTo(columnName, value)
                break
            case "notEqualTo":
                this.predicates.notEqualTo(columnName, value)
                break
        }
        return this;
    }

    public and(columnName: string, op: string, value: number | string | boolean): Selector {
        switch (op) {
            case "equalTo":
                this.predicates.and().equalTo(columnName, value)
                break
            case "notEqualTo":
                this.predicates.and().notEqualTo(columnName, value)
                break
        }
        return this;
    }

    public or(columnName: string, op: string, value: number | string | boolean): Selector {
        switch (op) {
            case "equalTo":
                this.predicates.or().equalTo(columnName, value)
                break
            case "notEqualTo":
                this.predicates.or().notEqualTo(columnName, value)
                break
        }
        return this;
    }

    /**
     * 配置谓词以匹配其值按升序排序的列
     * @param columnName
     */
    public orderByAsc(columnName: string): Selector {
        this.predicates.or().orderByAsc(columnName)
        return this;
    }

    /**
     * 配置谓词以匹配其值按降序排序的列
     * @param columnName
     */
    public orderByDesc(columnName: string): Selector {
        this.predicates.or().orderByDesc(columnName)
        return this;
    }

    public getQueryColumns(): Array<string> {
        return this.queryColumns
    }
}
