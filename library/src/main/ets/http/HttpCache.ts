/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class HttpCache {
    private DEFAULT_CACHE_SIZE = 1024 * 100; // string length
    private DEFAULT_EXPIRY_TIME = 1000 * 60; // 60 seconds
    private defaultExpiryTime = this.DEFAULT_EXPIRY_TIME;

    public setCacheSize(strLength: number) {

    }

    public setDefaultExpiryTime(defaultExpiryTime: number) {
        this.defaultExpiryTime = defaultExpiryTime;
    }

    public getDefaultExpiryTime(): number {
        return this.defaultExpiryTime;
    }
}
