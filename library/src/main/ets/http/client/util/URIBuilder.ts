/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class URIBuilder {
    private path: string;
    private headers: Map<string, any> ;
    private queryParams: Map<string, any> ;

    constructor(uri: string) {
        this.digestURI(uri);
    }

    digestURI(uri: string) {
        this.path = uri;
    }

    public getPath(): string {
        return this.path;
    }

    public addHeaders(param: Map<string, any>): URIBuilder {
        if (this.headers == null) {
            this.headers = new Map();
        }
        this.headers = param;
        return this;
    }

    public addParameter(param: Map<string, any>): URIBuilder {
        if (this.queryParams == null) {
            this.queryParams = new Map();
        }
        this.queryParams = param;
        return this;
    }

    public getHeaders(): Map<string, any> {
        if (this.headers != null) {
            return this.headers;
        } else {
            return new Map();
        }
    }

    public getQueryParams(): Map<string, any> {
        if (this.queryParams != null) {
            return this.queryParams;
        } else {
            return new Map();
        }
    }
}
