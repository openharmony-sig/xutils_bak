/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fileio from '@ohos.file.fs';
import { GlobalContext } from '../GlobalContext';
import { LruDiskCacheBean } from './LruDiskCacheBean'

export class LruDiskCache {
    FileName = "xBitmapCache";
    AppVersion: string = "1.0.0"
    MaxValue: number = 50 * 1024
    StorageSize: number = 0;
    IsDeleteAll: boolean = false
    mFilePath: string = "";
    global: GlobalContext = GlobalContext.getContext()

    public constructor(fileName?: string, appVersion?: string, maxValue?: number) {
        if (fileName != null) {
            this.FileName = fileName
        }
        if (appVersion != null) {
            this.AppVersion = appVersion
        }
        if (maxValue >= 0) {
            this.MaxValue = maxValue
        }

        this.initDiskCache()
    }

    public getAllImageName(): LruDiskCacheBean {

        let mDefaultLruDiskCacheBean: LruDiskCacheBean

        let Filepath = this.mFilePath + this.FileName + "/journal.txt";

        let decodedString = fileio.readTextSync(Filepath)

        mDefaultLruDiskCacheBean = new LruDiskCacheBean(decodedString);

        return mDefaultLruDiskCacheBean
    }

    public addImageItem(ImageName: string, ImageSize: number) {
        let CacheBean = this.getJournal();
        let mDefaultLruDiskCacheBean = new LruDiskCacheBean(CacheBean);
        mDefaultLruDiskCacheBean.setImageKey(ImageName);
        mDefaultLruDiskCacheBean.setStorageSize((mDefaultLruDiskCacheBean.getStorageSize() + ImageSize))
        let newValue = mDefaultLruDiskCacheBean.toJson();
        this.writeText(newValue);

    }

    public DeleteItemImage(ImagePath: string, ImageName: string, ImageSize: number) {

        let CacheBean = this.getJournal();
        let mDefaultLruDiskCacheBean = new LruDiskCacheBean(CacheBean);
        fileio.unlinkSync(ImagePath);
        mDefaultLruDiskCacheBean.deleteItemImageKey(ImageName);

        mDefaultLruDiskCacheBean.setStorageSize((mDefaultLruDiskCacheBean.getStorageSize() - ImageSize))
        let newValue = mDefaultLruDiskCacheBean.toJson();
        this.clearText();
        this.writeText(newValue)
    }

    public DeleteAllImage(isDeleteAll: boolean, path: string, isCache: boolean) {
        let mDefaultLruDiskCacheBean: LruDiskCacheBean
        if (isDeleteAll) {
            let readText = this.getJournal();
            mDefaultLruDiskCacheBean = new LruDiskCacheBean(readText);
            if (mDefaultLruDiskCacheBean.getImageKey().length >= 0) {
                mDefaultLruDiskCacheBean.getImageKey().forEach((val, idx) => {
                    if (this.getFileAccess(path + val)) {
                        fileio.unlinkSync(path + val)
                    }
                })
            }

            mDefaultLruDiskCacheBean.deleteAllImageKey()
            mDefaultLruDiskCacheBean.setStorageSize(0)

            let newValue = mDefaultLruDiskCacheBean.toJson();
            if (!isCache) {
                this.clearText();
                this.writeText(newValue)
            }

        }
    }

    public getFileAccess(path) {
        try {
            return fileio.accessSync(path);
        } catch (e) {
            return false
        }
    }

    public getFileMessage(path) {
        let stat = fileio.statSync(path)
        return stat
    }

    private writeText(value: string) {
        let fd = fileio.openSync(this.global.getValue('filesDir')  + "/" + this.FileName + "/journal.txt", fileio.OpenMode.READ_WRITE | fileio.OpenMode.CREATE);
        fileio.writeSync(fd.fd, value, { offset: 0, encoding: 'utf-8' });
        fileio.closeSync(fd)

    }

    private clearText() {
        fileio.unlinkSync(this.global.getValue('filesDir')  + "/" + this.FileName + "/journal.txt")
    }

    //初始化文件
    private initDiskCache() {
        this.mFilePath = this.global.getValue('filesDir') as string
        try {
            let exits = fileio.accessSync(this.global.getValue('filesDir')  + "/" + this.FileName + "/journal.txt");
            if (exits) {
                this.clearText()
                this.writeText('{"title": "libcore.io.DiskLruCache","cacheVersion": "1","appVersion": "' + this.AppVersion + '","maxValue": ' + this.MaxValue + ',"storageSize":' + this.StorageSize + ',"imageKey":[]}')
            } else {
                let fd = fileio.openSync(this.global.getValue('filesDir')  + "/" + this.FileName + "/journal.txt", fileio.OpenMode.READ_WRITE | fileio.OpenMode.CREATE);
                fileio.writeSync(fd.fd, '{"title": "libcore.io.DiskLruCache","cacheVersion": "1","appVersion": "' + this.AppVersion + '","maxValue": ' + this.MaxValue + ',"storageSize":' + this.StorageSize + ',"imageKey":[]}', {
                    offset: 0,
                    encoding: 'utf-8'
                });
                fileio.closeSync(fd)
            }
        } catch (err) {
            console.info("LruDiskCache accessSync failed with error message: " + err.message + ", error code: " + err.code);
        }
    }

    private getJournal(): string {
        let Filepath = this.mFilePath + "/" + this.FileName + "/journal.txt";
        let decodedString = fileio.readTextSync(Filepath)
        return decodedString;
    }
}