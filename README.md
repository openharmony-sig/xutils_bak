# xutils

## Introduction
> xutils is a utility library for network, file, and database operations.

![preview.gif](preview/preview_EN.gif)

## How to Install
```shell
ohpm install @ohos/xutils
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use
1. Import dependencies.

    ```
      import { HttpUtils } from '@ohos/xutils/'
      import { HttpMethod } from '@ohos/xutils/'
      import { RequestCallBack } from '@ohos/xutils/'
      import { RequestParams } from '@ohos/xutils/'
      import { ResponseInfo } from '@ohos/xutils/'
      import { DbUtils } from '@ohos/xutils/'
      import { DaoConfig } from '@ohos/xutils/'
      import { QueryCallBack } from '@ohos/xutils/'
      import { Selector } from '@ohos/xutils/'
      import { BitmapUtils } from '@ohos/xutils/'
      import { BitmapLoadCallBack } from '@ohos/xutils/'
    ```

2. Initiate a network request.

    ```
      // Add a header.
      @State requestParams: RequestParams = new RequestParams();
      this.requestParams.addHeader("Content-Type", "application/json");
      // Add parameters.
      this.requestParams.addQueryStringParameter("key", "397c9db4cb0621ad0313123dab416668");
      this.requestParams.addQueryStringParameter("city," "Beijing");
      // Send a GET request.
      new HttpUtils().send(HttpMethod.GET,
               "http://apis.juhe.cn/simpleWeather/query?key=397c9db4cb0621ad0313123dab416668&city=Beijing",
               new HttpCallBack())
      // Send a POST request.
      new HttpUtils().sendParams(HttpMethod.POST,
               "http://apis.juhe.cn/simpleWeather/query",
               this.requestParams,
               new HttpCallBack())
    ```

3. Trigger file download.

    ```
    new HttpUtils().download('https://5b0988e595225.cdn.sohucs.com/images/20200206/3ed7fb3b096f4c2b9d66bd65baaa6c0e.jpeg',
               '/hsh.jpeg', null)
    ```

4. Perform database operations

    ```
    // Create a database.
    this.db = DbUtils.create(this.config);
    // Create a table.
    this.config.setDbName(this.dbName)
    this.config.setTableName(this.tableName)
    this.config.setCreateTableSql("ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, AGE INTEGER, SALARY REAL, CODES BLOB")
    this.db.createTableIfNotExist();
    // Insert data.
    const valueBucket = {
               "NAME": "Lisa",
               "AGE": 18,
               "SALARY": 100.5,
               // @ts-ignore
               "CODES": new Uint8Array([1, 2, 3, 4, 5]),
             };
    this.db.save(valueBucket)
    // Query data.
    this.db.findAll(Selector.from(this.tableName, this.queryColumns), new DbCallBack());
    this.db.findAll(Selector.from(this.tableName, this.queryColumns)
               .where("NAME", "equalTo", "Lisa").and("AGE", "equalTo", 18), new DbCallBack());
    this.db.findFirst(Selector.from(this.tableName, this.queryColumns)
               .where("NAME", "equalTo", "Rose"), new DbCallBack());
    // Update data.
    this.db.update(valueBucket, Selector.from(this.tableName, this.queryColumns)
               .where("NAME", "equalTo", "Rose"));
    // Delete data.
    this.db.delete(Selector.from(this.tableName, this.queryColumns)
               .where("NAME", "equalTo", "Rose"));
    // Delete the database.
    this.db.dropDb();
    ```

## Available APIs
1. Sends a GET request.
   ` HttpUtils.send()`
2. Sends a POST request.
   `HttpUtils.sendParams()`
3. Downloads a file.
   `HttpUtils.download()`
4. Sets the database name.
   `DaoConfig.setDbName()`
5. Sets the table name.
   `DaoConfig.setTableName()`
6. Queries data.
   `DbUtils.findAll()`
7. Updates data.
   `DbUtils.update()`

## Constraints
This project has been verified in the following version:

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)
- DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK:API11 (4.1.0.36)


## Directory Structure
````
|---- xutils
      |   |---- entry  # Sample code
         |----entity
         |----pages
      |---- library  # xutils library
         |----src
            |----main
               |----ets
                      |---- bitmap # Image operation implementation
                      |---- cache # Cache implementation
                      |---- db # Database operation implementation
                      |---- http # Network request operation implementation
                      |---- task # Task implementation
                      |---- ts-md5 # Encryption implementation
                      |---- util # Utility class implementation
                      |---- BitmapUtils.ets # Image cache external classes
                      |---- DbUtils.ets # Database operation external classes
                      |---- HttpUtils.ets # Network request operation external classes
                      |---- index.ets  # External APIs
      |---- README.md  # Readme
      |---- README_zh.md  # Readme                   
````

## How to Contribute
If you find any problem during the use, submit an [issue](https://gitee.com/openharmony-sig/xutils/issues) or a [PR](https://gitee.com/openharmony-sig/xutils/pulls).

## License
This project is incensed under [Apache License 2.0](https://gitee.com/openharmony-sig/xutils/blob/master/LICENSE).
